﻿using System;

namespace LookingForArrayElements
{
    public static class DecimalCounter
    {
        public static int GetDecimalsCount(decimal[]? arrayToSearch, decimal[]?[]? ranges)
        {
            if (arrayToSearch == null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges == null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            for (int i = 0; i < ranges.Length; i++)
            {
                decimal[] range = ranges[i] ?? throw new ArgumentNullException(nameof(ranges));
                if (range.Length == 0)
                {
                    return 0;
                }

                if (range.Length != 2)
                {
                    throw new ArgumentException("Method throws ArgumentException in case the length of one of the ranges is less or greater than 2.", nameof(ranges));
                }

                if (range[0] > range[1])
                {
                    throw new ArgumentException("abc", nameof(ranges));
                }
            }

            int cnt = 0;
            for (int i = 0; i < arrayToSearch.Length; i++)
            {
                for (int j = 0; j < ranges.Length; j++)
                {
                    decimal[] range = ranges[j] ?? throw new ArgumentNullException(nameof(ranges));
                    if (arrayToSearch[i] >= range[0] && arrayToSearch[i] <= range[1])
                    {
                        cnt++;
                    }
                }
            }

            return cnt;
        }

        public static int GetDecimalsCount(decimal[]? arrayToSearch, decimal[]?[]? ranges, int startIndex, int count)
        {
            if (arrayToSearch == null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (ranges == null)
            {
                throw new ArgumentNullException(nameof(ranges));
            }

            for (int i = 0; i < ranges.Length; i++)
            {
                decimal[] range = ranges[i] ?? throw new ArgumentNullException(nameof(ranges));
                if (range.Length == 0)
                {
                    return 0;
                }

                if (range.Length != 2)
                {
                    throw new ArgumentException("Method throws ArgumentException in case the length of one of the ranges is less or greater than 2.", nameof(ranges));
                }

                if (range[0] > range[1])
                {
                    throw new ArgumentException("abc", nameof(ranges));
                }
            }

            if (startIndex + count > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(count));
            }

            int cnt = 0;
            for (int i = startIndex; i < startIndex + count; i++)
            {
                for (int j = 0; j < ranges.Length; j++)
                {
                    decimal[] range = ranges[j] ?? throw new ArgumentNullException(nameof(ranges));
                    if (arrayToSearch[i] >= range[0] && arrayToSearch[i] <= range[1])
                    {
                        cnt++;
                    }
                }
            }

            return cnt;
        }
    }
}
