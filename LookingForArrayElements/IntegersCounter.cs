﻿using System;

namespace LookingForArrayElements
{
    public static class IntegersCounter
    {
        public static int GetIntegersCount(int[]? arrayToSearch, int[]? elementsToSearchFor)
        {
            if (arrayToSearch == null)
            {
                throw new System.ArgumentNullException(nameof(arrayToSearch));
            }

            if (elementsToSearchFor == null)
            {
                throw new System.ArgumentNullException(nameof(elementsToSearchFor));
            }

            int sum = 0;
            for (int i = 0; i < arrayToSearch.Length; i++)
            {
                for (int j = 0; j < elementsToSearchFor.Length; j++)
                {
                    if (arrayToSearch[i] == elementsToSearchFor[j])
                    {
                        sum++;
                    }
                }
            }

            return sum;
        }

        public static int GetIntegersCount(int[]? arrayToSearch, int[]? elementsToSearchFor, int startIndex, int count)
        {
            if (elementsToSearchFor == null)
            {
                throw new System.ArgumentNullException(nameof(elementsToSearchFor));
            }

            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            if (arrayToSearch == null)
            {
                throw new ArgumentNullException(nameof(arrayToSearch));
            }

            if (startIndex > arrayToSearch.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(startIndex));
            }

            int sum = 0;
            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < elementsToSearchFor.Length; j++)
                {
                    if (arrayToSearch[i + startIndex] == elementsToSearchFor[j])
                    {
                        sum++;
                    }
                }
            }

            return sum;
        }
    }
}
